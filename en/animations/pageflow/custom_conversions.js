
function PageFlowAds(resources)
{
	PageFlowAds.resources = resources;
}
PageFlowAds.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 582, Phaser.AUTO, 'PageFlowAds', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},

	//var button;
    //var background;

	preload: function()
	{
		this.game.scale.maxWidth = 800;
    	this.game.scale.maxHeight = 582;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    	
    	this.game.load.image('pageico', PageFlowAds.resources.pageico);
    	this.game.load.image('userico', PageFlowAds.resources.userico);
    	this.game.load.image('arrow1', PageFlowAds.resources.arrow1);

    	this.game.load.image('greydot', PageFlowAds.resources.greydot);

    	this.game.load.image('neudot', PageFlowAds.resources.neudot);

       this.game.load.spritesheet('button', PageFlowAds.resources.pagebtnsprite, 200, 38);

    	this.game.load.image('bckg', PageFlowAds.resources.bckg);
    	this.game.stage.backgroundColor = '#000000'

	},

	create: function(evt)
	{
		
     //Background
     this.parent.bckg = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bckg');
     this.parent.bckg.anchor.set(0.5);
    //break
   



	//Header Icon
	this.parent.headerico = this.game.add.sprite(0,15, 'neudot');

	//Arrow pointer
	this.parent.arrow1 = this.game.add.sprite(300,75, 'arrow1');
	//this.parent.button1 = this.game.add.sprite(300,300, 'button');
    // this.parent.button1 = game.add.button(300, 400, 'button', actionOnClick, this, 2, 1, 0);

	//Big Page icon in a circle
    this.parent.pageico = this.game.add.sprite(300,-540, 'pageico');
    this.parent.pageico.anchor.set(0.5);
    //var button;

    //Call the animation build function
    this.parent.buildAnimation();
    
	},

	actionOnClick: function(evt){

      // this.para_1.alpha = 0;

	   this.game.time.events.remove(this.cycleArray[0]);
       
       console.log("Action Click" + evt.btnId);

       var aAo = evt.btnId;

       this.para_1.setText(this.paraTextArray[aAo]);
       
      // this.para_1.alpha = 0;

       var yLoc = this.arrowArray[aAo];

       this.tweenArrow = this.game.add.tween(this.arrow1).to( { x:300, y: yLoc -5 }, 1000, Phaser.Easing.Exponential.Out, true);

       this.tweenIcon = this.game.add.tween(this.userico).to( { x:19, y: yLoc }, 30, Phaser.Easing.Exponential.Out, true);

       //this.para_1.alpha = 0;
       //this.tweenPara = this.game.add.tween(this.para_1).to( { alpha:1 }, 3000, Phaser.Easing.Exponential.Out, true);

	},




	cycleData: function(evt)
			{

		var aAo = 0;	
       //console.log("Action Click" + evt.btnId);
		if(this.countInc <2){
         aAo = this.countInc += 1;
		}else{
		this.countInc = 0;	
		aAo = 0;
		}

       this.para_1.setText(this.paraTextArray[aAo]);

       var yLoc = this.arrowArray[aAo] ;

       var tweenArrow = this.game.add.tween(this.arrow1).to( { x:300, y: yLoc - 5 }, 1000, Phaser.Easing.Exponential.Out, true);

       var tweenIcon = this.game.add.tween(this.userico).to( { x:19, y: yLoc }, 30, Phaser.Easing.Exponential.Out, true);
	},


	up: function(evt)
			{
			//	debugger
		    //console.log('button up', arguments);
	}
		,
	over: function(evt)
			{
		    //console.log('button over');
	}
		,
	out: function(evt)
			{
		    //console.log('button out');
	}
	,

	createBall: function(evt) {

		console.log("Hello Ball");

    }
    ,

	buildAnimation: function()
	{

		//this.userico.width

		this.countInc = 0;

		this.arrowArray = new Array(80, 168, 258, 348, 438);


/*

		var tween1 = this.game.add.tween(this.userico1).to({x: 48, y: 85}, 500, Phaser.Easing.Exponential.Out, true);

		var tween2 = this.game.add.tween(this.userico2).to({x: 48, y: 175}, 1000, Phaser.Easing.Exponential.Out, true);

		var tween3 = this.game.add.tween(this.userico3).to({x: 48, y: 265}, 1500, Phaser.Easing.Exponential.Out, true);

		var tween4 = this.game.add.tween(this.userico4).to({x: 48, y: 355}, 2000, Phaser.Easing.Exponential.Out, true);

		var tween5 = this.game.add.tween(this.userico5).to({x: 48, y: 445}, 2500, Phaser.Easing.Cubic.Out, true).*/

	
        ///STYLES

        var style = {font:"bold 24px freight-sans-pro", fill: "#000000", wordWrap: false,wordWrapWidth: 500, align: "left"};

        var styleLeft = {font:"bold 24px freight-sans-pro", fill: "#000000", align: "Left",lineSpacing: -10 };

      
        var iconRight = {font: "24px Arial", fill: "#000000", align: "right"};
        iconRight.align = "left";

          var headerCenter = {font: "48px Arial", fill: "#FFFFFF"};

        //PARAGRAPH TEXT

	    this.paraTextArray = new Array(PageFlowAds.resources.para1text,PageFlowAds.resources.para2text,PageFlowAds.resources.para3text, PageFlowAds.resources.para4text, PageFlowAds.resources.para5text);

    	var styleRightMiddle = {font: "22px Arial", fill: "#000000", align: "left", boundsAlignH: "top", boundsAlignV: "right", wordWrap: true, wordWrapWidth: 435 };


   		this.para_1 = this.game.add.text(350, 90, PageFlowAds.resources.para1text, styleRightMiddle);

        //headerCenter.boundsAlignH ='center']
		///BUILDING BUTTONS IN LOOP

		this.butArray = new Array();

		for (var i=0;i<3;i++){
		var temp = this.game.add.button(20, 80 + (i * 90), 'button', this.actionOnClick, this, 1, 0, 2);
		    temp.btnId = i;
		    temp.alpha = 0;
		    this.butArray.push(temp);
		};

		this.userico = this.game.add.sprite(19,78, 'userico');

		this.userico.alpha = 0;

		
		
		//"headertext":"PAGES",
		//HEADER TEXT ADDED TO SgruntPRITE
		var header_1 = this.game.make.text(400, 20, PageFlowAds.resources.headertext, headerCenter);
		header_1.anchor.set(0.5);

		//header_1.y = 10; 
		//this.userico1.height-20;
		//header_1.x = 320; //this.userico1.width + 20;

		this.headerico.addChild(header_1);

		//this.arrowArray = new Array(78, 168, 258, 348, 438);

	    this.userico1 = this.game.add.sprite(-200,50 , 'neudot');
		this.userico2 = this.game.add.sprite(-200,170, 'neudot');
		this.userico3 = this.game.add.sprite(-200,260, 'neudot');
		//this.userico4 = this.game.add.sprite(-200,350, 'neudot');
		//this.userico5 = this.game.add.sprite(-200,420, 'neudot');

		//FEILD FOR USER ROLE ICONS

		var field_1 = this.game.make.text(0, 0, PageFlowAds.resources.ico1text, iconRight);
		field_1.x = this.userico1.width + 20;
		this.userico1.addChild(field_1);

		var field_2 = this.game.make.text(0, 0, PageFlowAds.resources.ico2text, iconRight);
		field_2.x = this.userico2.width + 20;
		this.userico2.addChild(field_2);

		var field_3 = this.game.make.text(0, 0, PageFlowAds.resources.ico3text, iconRight);
		field_3.x = this.userico3.width + 20;
		this.userico3.addChild(field_3);

		//var field_4 = this.game.make.text(0, 0, PageFlowAds.resources.ico4text, iconRight);
		//field_4.x = this.userico4.width + 20;
		//this.userico4.addChild(field_4);

		//var field_5 = this.game.make.text(0, 0, PageFlowAds.resources.ico5text, iconRight);
		//field_5.x = this.userico5.width + 20;
		//this.userico5.addChild(field_5);

		/////////////////////
		/////////////////////

		//this.pfield_1 = this.game.add.text(137, 600, PageFlowAds.resources.page1text, style);
		//this.pfield_2 = this.game.add.text(390, 600, PageFlowAds.resources.page2text, style);
		//this.pfield_3 = this.game.add.text(610, 600, PageFlowAds.resources.page3text, style);
		//this.pfield_4 = this.game.add.text(137, 675, PageFlowAds.resources.page4text, style);
		//this.pfield_5 = this.game.add.text(390, 675, PageFlowAds.resources.page5text, style);
		//this.pfield_6 = this.game.add.text(610, 675, PageFlowAds.resources.page6text, style);

		this.animate();

	},

	animate: function()
	{

	//ANIMATIONS
	//var tweenPageIco = this.game.add.tween(this.pageico).to( { x:300, y: 540 }, 1000, Phaser.Easing.Exponential.Out, true);

	/////////this.arrowArray = new Array(78, 168, 258, 348, 438);

		var tween1 = this.game.add.tween(this.userico1).to({x: 45, y: 85}, 500, Phaser.Easing.Exponential.Out, true);

		var tween2 = this.game.add.tween(this.userico2).to({x: 45, y: 175}, 1000, Phaser.Easing.Exponential.Out, true);

		var tween3 = this.game.add.tween(this.userico3).to({x: 45, y: 265}, 1500, Phaser.Easing.Exponential.Out, true).
                    onComplete.add(function () {
                        console.log('TIMER COMPLETE');
                        this.nextAnim();
                    }, this);

		///var tween4 = this.game.add.tween(this.userico4).to({x: 45, y: 355}, 2000, Phaser.Easing.Exponential.Out, true);

		//var tween5 = this.game.add.tween(this.userico5).to({x: 45, y: 445}, 2500, Phaser.Easing.Cubic.Out, true).
                    //onComplete.add(function () {
                       // console.log('TIMER COMPLETE');
                      //  this.nextAnim();
                    //}, this);
	
	},

	nextAnim: function(evt)
	{
		console.log("DELAYED");

        var tweenUseIcon = this.game.add.tween(this.userico).to( { alpha:1}, 1000, Phaser.Easing.Exponential.In, true);

		//var tweenArrow = this.game.add.tween(this.arrow1 ).to( { x:300, y: 75 }, 1000, Phaser.Easing.Exponential.Out, true);

	

		for (var i=0;i<3;i++){
				var temp =  this.butArray[i];
				var tweenTemp = this.game.add.tween(temp).to({alpha: 1}, 500 + (i*500), Phaser.Easing.Exponential.In, true);
		};


        this.cycleArray = new Array();
        this.cycleArray[0] = this.game.time.events.repeat(Phaser.Timer.SECOND * 4, 100, this.cycleData, this);


	},

/////////////////////////////////////////////////

	update: function()
	{

	},

	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}